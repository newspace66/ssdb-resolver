package resolver

import (
	"errors"
	"time"

	ssdbclient "gitee.com/newspace66/ssdb-resolver/ssdbclient"
)

type SsdbClient struct {
	mainClient *ssdbclient.SsdbClient
	bakClient  *ssdbclient.SsdbClient
}

const (
	SSDB_OK        = "ok"
	SSDB_NOT_FOUND = "not_found"
)

var ErrNil = errors.New("nil val")

func Init(addr string, auth string, addrBak string, authBak string, dialTimeout, deadlineTimeout time.Duration) *SsdbClient {

	ssdbClient := &SsdbClient{
		mainClient: ssdbclient.Init(addr, auth, dialTimeout, deadlineTimeout),
		bakClient:  ssdbclient.Init(addrBak, authBak, dialTimeout, deadlineTimeout),
	}
	return ssdbClient
}

func (c *SsdbClient) Set(key string, val string) (v interface{}, err error) {
	v, err = c.mainClient.Set(key, val)
	if err != nil {
		v, err = c.bakClient.Set(key, val)
	}
	return v, err
}

func (c *SsdbClient) Get(key string) (v string, err error) {
	v, err = c.mainClient.Get(key)
	if err != nil {
		v, err = c.bakClient.Get(key)
	}
	return v, err
}
func (c *SsdbClient) Del(key string) (v string, err error) {
	v, err = c.mainClient.Del(key)
	if err != nil {
		v, err = c.bakClient.Del(key)
	}
	return v, err
}

func (c *SsdbClient) Expire(key string, ttl int) (v interface{}, err error) {
	v, err = c.mainClient.Expire(key, ttl)
	if err != nil {
		v, err = c.bakClient.Expire(key, ttl)
	}
	return v, err
}
func (c *SsdbClient) SetNx(key string, value string, ttl int) (v string, err error) {
	v, err = c.mainClient.SetNx(key, value, ttl)
	if err != nil {
		v, err = c.bakClient.SetNx(key, value, ttl)
	}
	return v, err
}

func (c *SsdbClient) SetEx(key string, value string, ttl int) (v string, err error) {
	v, err = c.mainClient.SetEx(key, value, ttl)
	if err != nil {
		v, err = c.bakClient.SetEx(key, value, ttl)
	}
	return v, err
}

func (c *SsdbClient) Incr(key string) (v string, err error) {
	v, err = c.mainClient.Incr(key)
	if err != nil {
		v, err = c.bakClient.Incr(key)
	}
	return v, err
}

func (c *SsdbClient) HSet(key1 string, key2 string, value string) (v string, err error) {
	v, err = c.mainClient.HSet(key1, key2, value)
	if err != nil {
		v, err = c.bakClient.HSet(key1, key2, value)
	}
	return v, err
}

func (c *SsdbClient) HGet(key1 string, key2 string) (v string, err error) {
	v, err = c.mainClient.HGet(key1, key2)
	if err != nil {
		v, err = c.bakClient.HGet(key1, key2)
	}
	return v, err
}

func (c *SsdbClient) HGetAll(key1 string) (v map[string]string, err error) {
	v, err = c.mainClient.HGetAll(key1)
	if err != nil {
		v, err = c.bakClient.HGetAll(key1)
	}
	return v, err
}

func (c *SsdbClient) HExists(key1 string, key2 string) (v string, err error) {
	v, err = c.mainClient.HExists(key1, key2)
	if err != nil {
		v, err = c.bakClient.HExists(key1, key2)
	}
	return v, err
}

func (c *SsdbClient) HKeys(key string) (values []string, err error) {
	values, err = c.mainClient.HKeys(key)
	if err != nil {
		values, err = c.bakClient.HKeys(key)
	}
	return values, err
}

func (c *SsdbClient) HDel(key1 string, key2 string) (v string, err error) {
	v, err = c.mainClient.HDel(key1, key2)
	if err != nil {
		v, err = c.bakClient.HDel(key1, key2)
	}
	return v, err
}

func (c *SsdbClient) HScan(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	datas, err = c.mainClient.HScan(name, keystart, keyend, limit)
	if err != nil {
		datas, err = c.bakClient.HScan(name, keystart, keyend, limit)
	}
	return datas, err
}
func (c *SsdbClient) HRScan(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	datas, err = c.mainClient.HRScan(name, keystart, keyend, limit)
	if err != nil {
		datas, err = c.bakClient.HRScan(name, keystart, keyend, limit)
	}
	return datas, err
}

func (c *SsdbClient) HScanWithScore(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	datas, err = c.mainClient.HScanWithScore(name, keystart, keyend, limit)
	if err != nil {
		datas, err = c.bakClient.HScanWithScore(name, keystart, keyend, limit)
	}
	return datas, err
}

func (c *SsdbClient) HRScanWithScore(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	datas, err = c.mainClient.HRScanWithScore(name, keystart, keyend, limit)
	if err != nil {
		datas, err = c.bakClient.HRScanWithScore(name, keystart, keyend, limit)
	}
	return datas, err
}
