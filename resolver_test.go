package resolver_test

import (
	"fmt"
	"testing"
	"time"

	resolver "gitee.com/newspace66/ssdb-resolver"
)

var dbInstance *resolver.SsdbClient

func init() {
	dbInstance = resolver.Init("9.134.15.18:15001", "YZF20190311ssdbPassWordYMustInput", "9.134.15.18:15002", "YZF20190311ssdbPassWordYMustInput", 3*time.Second, 3*time.Second)
}

func TestSet(t *testing.T) {
	v, err := dbInstance.Set("test", "testvalue")
	if err != nil {
		fmt.Printf("error: %v\n", err)
		t.Fatalf("expected an error")
	}
	fmt.Println("Set", v, err)
}

func TestGet(t *testing.T) {
	v, err := dbInstance.Get("test")
	if err != nil {
		fmt.Printf("error: %v\n", err)
		t.Fatalf("expected an error")
	}
	fmt.Println("Get", v, err)
}

func TestHSet(t *testing.T) {
	v, err := dbInstance.HSet("config", "name", "tt")
	v, err = dbInstance.HSet("config", "age", "11")
	if err != nil {
		fmt.Printf("error: %v\n", err)
		t.Fatalf("expected an error")
	}
	fmt.Println("Get", v, err)
}

func TestHGetAll(t *testing.T) {
	v, err := dbInstance.HGetAll("config")
	fmt.Println("Get", v, err)
	if err != nil {
		fmt.Printf("error: %v\n", err)
		t.Fatalf("expected an error")
	}

}
