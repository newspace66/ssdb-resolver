package ssdbclient

import (
	"errors"
	"fmt"
	"time"

	ssdb "gitee.com/newspace66/ssdb-resolver/ssdb"
)

const (
	SSDB_OK        = "ok"
	SSDB_NOT_FOUND = "not_found"
)

type SsdbClient struct {
	addr            string
	auth            string
	flag            int
	connErrCount    int64
	dialTimeout     time.Duration
	deadlineTimeout time.Duration
}

func Init(addr string, auth string, dialTimeout, deadlineTimeout time.Duration) *SsdbClient {
	if dialTimeout == 0 {
		dialTimeout = time.Second * 3
	}
	if deadlineTimeout == 0 {
		deadlineTimeout = time.Second * 3
	}
	ssdbClient := &SsdbClient{
		addr:            addr,
		auth:            auth,
		flag:            0,
		connErrCount:    0,
		dialTimeout:     dialTimeout,
		deadlineTimeout: deadlineTimeout,
	}
	return ssdbClient
}

func (client *SsdbClient) getConnec() (*ssdb.Client, error) {
	var err error
	var conn *ssdb.Client

	conn, err = ssdb.Connect(client.addr, client.dialTimeout, client.deadlineTimeout)
	if err != nil {
		return nil, err
	}
	if client.auth != "" {
		_, err := conn.Do("auth", client.auth)
		if err != nil {
			return nil, err
		}
	}
	return conn, err
}

func (c *SsdbClient) Set(key string, val string) (interface{}, error) {
	conn, err := c.getConnec()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	resp, err := conn.Do("set", key, val)
	if err == nil {
		if len(resp) == 2 && resp[0] == SSDB_OK {
			return resp[1], nil
		}
	}
	return nil, err
}

func (c *SsdbClient) Get(key string) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("get", key)
	if err != nil || len(resp) == 0 {
		return "", err
	}
	if len(resp) == 2 && resp[0] == SSDB_OK {
		return resp[1], nil
	}
	if resp[0] == SSDB_NOT_FOUND {
		return "", nil
	}
	return "", fmt.Errorf("bad response, err:" + resp[0])
}

func (c *SsdbClient) Del(key string) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("del", key)
	if err != nil {
		return "", err
	}
	if len(resp) > 0 && resp[0] == SSDB_OK {
		return resp[1], nil
	}
	return "", fmt.Errorf("bad response:resp:%v:", resp)
}
func (c *SsdbClient) Expire(key string, ttl int) (interface{}, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("expire", key, ttl)
	if err != nil {
		return "", err
	}
	if len(resp) == 2 && resp[0] == SSDB_OK {
		return true, nil
	}
	return "", errors.New("bad response")
}
func (c *SsdbClient) SetNx(key string, value string, ttl int) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("setnx", key, value)
	if err != nil {
		return "0", err
	}
	if len(resp) == 2 && resp[0] == SSDB_OK {
		_, err = conn.Do("expire", key, ttl)
		if err != nil {
		}
		return resp[1], nil
	}
	return "0", fmt.Errorf("bad response")
}

func (c *SsdbClient) SetEx(key string, value string, ttl int) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("set", key, value)
	if err != nil {
		return "", err
	}
	if len(resp) == 2 && resp[0] == SSDB_OK {
		_, err = conn.Do("expire", key, ttl)
		if err != nil {
		}
		return resp[1], nil
	}
	return "", fmt.Errorf("bad response")
}

func (c *SsdbClient) Incr(key string) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("incr", key)
	if err != nil {
		return "", err
	}

	if len(resp) > 0 && resp[0] == SSDB_OK {
		return resp[1], nil
	}
	return "", fmt.Errorf("bad response:resp:%v:", resp)
}

func (c *SsdbClient) HSet(key1 string, key2 string, value string) (string, error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("hset", key1, key2, value)
	if err != nil {
		return "", err
	}
	if len(resp) == 2 && resp[0] == SSDB_OK {
		return resp[1], nil
	}
	return "", errors.New("bad response")
}

func (c *SsdbClient) HGet(key1 string, key2 string) (result string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("hget", key1, key2)
	if err != nil {
		return "", err
	}
	if resp[0] == SSDB_OK {
		return resp[1], nil
	}
	if resp[0] == SSDB_NOT_FOUND {
		return "", nil
	}
	return "", errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HGetAll(key1 string) (result map[string]string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return result, err
	}
	defer conn.Close()
	resp, err := conn.Do("hgetall", key1)
	if err != nil {
		return result, err
	}
	fmt.Println(resp)
	result = make(map[string]string)
	for i := 1; i < len(resp); i = i + 2 {
		result[resp[i]] = resp[i+1]
	}
	if resp[0] == SSDB_OK {
		return result, nil
	}
	if resp[0] == SSDB_NOT_FOUND {
		return result, nil
	}

	return result, errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HExists(key1 string, key2 string) (result string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return "", err
	}
	defer conn.Close()
	resp, err := conn.Do("hexists", key1, key2)
	if err != nil {
		return "", err
	}
	if resp[0] == SSDB_OK {
		return resp[1], nil
	}
	return "", errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HKeys(key string) (values []string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return values, err
	}
	defer conn.Close()
	resp, err := conn.Do("hkeys", key, "", "", 100)
	if err != nil {
		return values, err
	}
	if resp[0] == SSDB_OK {
		return resp[1:], nil
	}
	if resp[0] == SSDB_NOT_FOUND {
		return values, nil
	}
	return values, errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HDel(key1 string, key2 string) (v string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return v, err
	}
	defer conn.Close()
	resp, err := conn.Do("hdel", key1, key2)
	if err != nil {
		return "", err
	}
	if resp[0] == SSDB_OK {
		return resp[1], nil
	}
	return "", errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HScan(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return datas, err
	}
	defer conn.Close()

	//显式初始化数组，防止外面json化出现null
	datas = make([]string, 0)
	rsp, err := c.HScanWithScore(name, keystart, keyend, limit)
	if err != nil {
		return datas, err
	}

	size := len(rsp)
	for i := 1; i < size; i += 2 {
		datas = append(datas, rsp[i])
	}

	return datas, nil
}
func (c *SsdbClient) HRScan(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return datas, err
	}
	defer conn.Close()

	//显式初始化数组，防止外面json化出现null
	datas = make([]string, 0)
	rsp, err := c.HRScanWithScore(name, keystart, keyend, limit)
	if err != nil {
		return datas, err
	}

	size := len(rsp)
	for i := 1; i < size; i += 2 {
		datas = append(datas, rsp[i])
	}

	return datas, nil
}

func (c *SsdbClient) HScanWithScore(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return datas, err
	}
	defer conn.Close()
	resp, err := conn.Do("hscan", name, keystart, keyend, limit)
	if err != nil {
		return datas, err
	}
	if resp[0] == SSDB_OK {
		return resp[1:], nil
	}
	return datas, errors.New("bad response,err:" + resp[0])
}

func (c *SsdbClient) HRScanWithScore(name string, keystart string, keyend string, limit int) (datas []string, err error) {
	conn, err := c.getConnec()
	if err != nil {
		return datas, err
	}
	defer conn.Close()
	resp, err := conn.Do("hrscan", name, keystart, keyend, limit)
	if err != nil {
		return datas, err
	}
	if resp[0] == SSDB_OK {
		return resp[1:], nil
	}
	return datas, errors.New("bad response,err:" + resp[0])
}
